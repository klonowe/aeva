# aeva_build_documentation is an enumerated option:
# never  == No documentation, and no documentation tools are required.
# manual == Only build when requested; documentation tools (doxygen and
#           sphinx) must be located during configuration.
# always == Build documentation as part of the default target; documentation
#           tools are required. This is useful for automated builds that
#           need "make; make install" to work, since installation will fail
#           if no documentation is built.
set(aeva_build_documentation
  "never" CACHE STRING "When to build Doxygen- and Sphinx-generated documentation.")
set_property(CACHE aeva_build_documentation PROPERTY STRINGS never manual always)
if (NOT aeva_build_documentation STREQUAL "never")
  find_package(Sphinx)
endif()

option(aeva_enable_testing "Build aeva Testing" ON)
