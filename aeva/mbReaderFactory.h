//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
/**
 * @class   mbReaderFactory
 * @brief   is a factory for creating a reader
 * proxy based on the filename/extension.
 *
 * mbReaderFactory is an extension of vtkSMReaderFactory to facilitate the
 * modification of the presented file reader list to filter non-SMTK readers
 * when post-processing mode is disabled.
*/
#ifndef aeva_mbReaderFactory_h
#define aeva_mbReaderFactory_h

#include "vtkSMReaderFactory.h"

class mbReaderFactory : public vtkSMReaderFactory
{
public:
  static mbReaderFactory* New();
  vtkTypeMacro(mbReaderFactory, vtkSMReaderFactory);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Toggle available readers to include ParaView's postprocessing readers.
   */
  void SetPostProcessingMode(bool choice);

  /**
   * Returns a formatted string with all supported file types.
   * \c cid is not used currently.
   * An example returned string would look like:
   * \verbatim
   * "Supported Files (*.vtk *.pvd);;PVD Files (*.pvd);;VTK Files (*.vtk)"
   * \endverbatim
   */
  const char* GetSupportedFileTypes(vtkSMSession* session) override;

protected:
  mbReaderFactory();
  ~mbReaderFactory() override;

  void UpdateAvailableReaders() override;

private:
  mbReaderFactory(const mbReaderFactory&) = delete;
  void operator=(const mbReaderFactory&) = delete;

  class vtkInternals;
  vtkInternals* Internals;
};

#endif
