.. _aeva User's Guide:

================
aevaCMB User's Guide
================

Navigating aevaCMB Interface
=========================

The aevaCMB window inherits the interface of `ParaView <https://www.paraview.org/>`_. It contains a RenderView, panels, the main toolbar, and the RenderView toolbar. In addition, a handful of panels and toolbars are customized to facilitate annotations on anatomy.

.. image:: figures/figure7.png
  :width: 600

RenderView
-----------

* The RenderView is the visualization area.
* Multiple Renderviews may be open simultaneously. Create a new view by:

  #. clicking the + to open a new Layout tab, or
  #. Selecting "Split Horizontal" or "Split Vertical"

Panels
------

* Panel visibility may be turned on and off under 'View'.
* Panels may be dragged and dropped to arrange them in the window by user preference.
* See aevaCMB Panels below for more detail.

Main Toolbar
------------

* Main toolbar is located at the top of the window.
* The tool bar ontains viewing options (left), selection options (middle), and aevaCMB operation shortcuts (right).

RenderView Toolbar
------------------

* RenderView toolbar is located at the top of the RenderView.
* The toolbar contains camera options (left), cell selection tools (middle), and RenderView options (right).

aevaCMB Concepts
=============

Resources
---------

Resources are containers to store annotations or geometries.

Attributes
----------

Attributes are features (properties, ontologies, etc.) that can be associated with geometry resources and stored as annotations.

Operations
----------

Operations are actions performed on a resource (primarly geometry) and include selection of regions, mesh operations, export, freeform annotation, etc.

aevaCMB Panels
===========

Attribute Editor
----------------

Attribute Editor provides a gateway to template annotations that can be assigned to geometry resources, e.g. tissue type, organ, etc.

Operations
----------

Operations provide a list of all available actions for the selected resource or its regions (defined sets). One can view all operations by unchecking "limit by selection". When an operation is selected (either from the operation panel or using one of the toolbar shortcuts), relevant Operation editor will appear under the Operations panel where the user can provide inputs. See aevaCMB Operations for a list.

Output Messages
---------------

When operations are performed, output messages appear in this panel to inform the user of success or failure.

Properties
----------

This panel is inherited from Paraview. It can help change visualization settings for a given resource.

aevaCMB Operations
===============

A variety of operations are categorized with access points:

* Region selection and editing

  * At RenderView toolbar

    * Select Cells On
    * Select Points On
    * Select Cells with Polygon
    * Select Points with Polygon

  * At Main toolbar

    * Grow to edges
    * Select by surface normal
    * Select by proximity
    * Select by adjacency
    * Select all surface primitives
    * Select the nodes of all selected primitives
    * Duplicate cells
    * Intersect side sets
    * Subtract side sets
    * Unite side sets
    * Unreferenced primitives

* Annotation

  * At Operations panel

    * Associate
    * Dissociate
    * Edit freeform attributes

* Mesh/geometry modification

  * At Main toolbar

    * Reconstruct surface
    * Smooth surface
    * Quadratic promotion
    * Proportional Edit
    * Imprint Geometry

  * At Operations panel

    * NetgenSurfaceRemesher
    * VolumeMesher

* File loading, import/export

  * At File drop-down menu

    * Open...
    * Import Into Resource...
    * Save Resource
    * Save Resource As..
    * Load All Resources...
    * Save All Resources As...

  * At Operation panel

    * Import
    * Export
